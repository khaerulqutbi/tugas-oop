<?php 
	require_once("animal.php");
	require_once("frog.php");
	require_once("ape.php");

	$binatangku = new animal("Sapi");

	echo "Nama : " . $binatangku->nama . "<br>";
	echo "Legs : " .  $binatangku->legs . "<br>";
	echo "Cold Blooded : " . $binatangku->cold_blooded;

	$katak = new frog("Katak");
	echo "Nama : " . $katak->nama . "<br>";
	echo "Legs : " .  $katak->legs . "<br>";
	echo "Cold Blooded : " . $katak->cold_blooded . "<br>";
	echo "Jump : ";
	$katak->jump();

	$kera = new ape("Kera");
	echo "Nama : " . $kera->nama . "<br>";
	echo "Legs : " .  $kera->legs . "<br>";
	echo "Cold Blooded : " . $kera->cold_blooded . "<br>";
	echo "Yell : ";
	$kera->yell();



 ?>